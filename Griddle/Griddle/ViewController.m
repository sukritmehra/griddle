//
//  ViewController.m
//  Griddle
//
//  Created by Sukrit Mehra on 18/05/14.
//  Copyright (c) 2014 Griddle, Inc. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"
#import "RegisterViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self.navigationController.navigationBar setHidden:YES];
    [self.view setBackgroundColor:[UIColor colorWithRed:0.318 green:0.698 blue:0.914 alpha:1]];
    [self.navigationController.navigationBar setHidden:TRUE];
    
    /*
    // Check fb delegates
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    if (!appDelegate.session.isOpen) {
        // create a fresh session object
        appDelegate.session = [[FBSession alloc] init];
        
        if (appDelegate.session.state == FBSessionStateCreatedTokenLoaded) {
            // even though we had a cached token, we need to login to make the session usable
            [appDelegate.session openWithCompletionHandler:^(FBSession *session, FBSessionState status, NSError *error)
             {
                 // we recurse here, in order to update buttons and labels
                 [self updateView];
             }];
        }
    }*/
}

#pragma mark - Buttons
-(IBAction)facebookBtnClicked:(id)sender
{/*
    // get the app delegate so that we can access the session property
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    
    // this button's job is to flip-flop the session from open to closed
    if (appDelegate.session.isOpen)
    {
        [appDelegate.session closeAndClearTokenInformation];
    }
    else
    {
        if (appDelegate.session.state != FBSessionStateCreated)
        {
            // Create a new, logged out session.
            appDelegate.session = [[FBSession alloc] init];
        }
        
        // if the session isn't open, let's open it now and present the login UX to the user
        [appDelegate.session openWithCompletionHandler:^(FBSession *session, FBSessionState status, NSError *error)
         {
             // and here we make sure to update our UX according to the new session state
             [self updateView];
         }];
    }*/
}

-(IBAction)registerBtnClicked:(id)sender
{
    RegisterViewController *rg = [[RegisterViewController alloc] initWithNibName:@"RegisterViewController" bundle:nil];
    
    [self.navigationController pushViewController:rg animated:YES];
}
-(IBAction)loginBtnClicked:(id)sender
{

}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    [self.navigationController.navigationBar setHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Facebook login response from server
- (void)updateView
{
    // get the app delegate, so that we can reference the session property
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    if (appDelegate.session.isOpen)
    {
        // valid account UI is shown whenever the session is open
        [facebookBtn setTitle:@"Logout" forState:UIControlStateNormal];
        
        // Facebook Access Token
        NSString *fbaccessToken = appDelegate.session.accessTokenData.accessToken;
        // User detail access with the url and access token
        NSString *urlString = [NSString stringWithFormat:@"https://graph.facebook.com/me?access_token=%@",[fbaccessToken stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        
        NSURL *url = [NSURL URLWithString:urlString];
        
        NSURLRequest * urlRequest = [NSURLRequest requestWithURL:url];
        NSURLResponse * response = nil;
        NSError * error = nil;
        // Contention made with facebook graph api and user details stored here
        NSData * data = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:&response error:&error];
        
        // Store user Details from mydata to dictionary with there key and values
        NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:Nil];
        
        NSString *FBName = [dictionary valueForKey:@"name"];
        // FB user-id if username is not present
        NSString *FBUserIDs = [dictionary valueForKey:@"id"];
        NSString *FBUserName = [dictionary valueForKey:@"username"];
        NSString *FBUserGender = [dictionary valueForKey:@"gender"];
        NSString *FBUserDOB = [dictionary valueForKey:@"birthday"];
        NSString *FBUserEmail = [dictionary valueForKey:@"email"];
        
        
    }
    else
    {
        // login-needed account UI is shown whenever the session is closed
        [facebookBtn setTitle:@"Register with Facebook" forState:UIControlStateNormal];
    }
}
*/
@end
